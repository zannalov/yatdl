# Yet Another To Do List

Yes... this is **yet another** to do list.

I found myself wanting a list which would let me set relative priority of
tasks. That is to say, "X is more important than Y" instead of just positioning
X above Y.

I also wanted to be able to say **why** one task is more important than
another. For example, "My boss says X is more important than Y." That way when
my boss's boss asks me to make Y more important than X, I can make an informed
decision.

I also don't like how most of the task managers out there expect you to store
all of your data in their servers.

I looked around and didn't see any obvious choices that met my desires, so I
decided to build my own.
