Phase 1
* Set up yatdl.com DNS and SSL cert
* React
  * https://www.npmjs.com/package/deep-freeze-strict
* Google Webmaster tools
* Google Analytics
* Icon
* Auto-saved to localStorage
* `type:'description'`, `id`, `summary`, `description` (usable with `task` and `importance`)
* `type:'task'`, `id`, `nextDue`, `created`
* `type:'importance'`, `id`, `greaterImportanceTaskId`, `lesserImportanceTaskId`
* Actions:
    * Load JSON file
    * Create Task
    * Delete Task
    * Set Importance
    * Remove Importance
    * Save JSON file
* Conflict/loop detection and resolution
* Sort order stabilization (if needed)
* CSS animation for reordered tasks
* Order by Importance, `nextDue` descending, `created` ascending
* Inline help bubbles (unicode info circle)

Future features
* Track sequencing (X must be completed before Y)
* Hide tasks (show a thin bar with a number indicating how many hidden tasks are at that location in the list)
* Undo/Redo
* Start date
* Recurrance patterns
* Save/Load from Google Drive
* Description Templates
* Keyboard shortcuts
* Separate/multiple lists
* Internationalization
* Accessibility
* Code complexity restrictions (standard doesn't let us set these, don't want to set up an entire installation of eslint just to add a few rules) http://eslint.org/docs/rules/complexity
