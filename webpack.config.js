/*
yatdl.com - Yet Another To Do List
Copyright (C) 2017 Jason Schmidt

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin')

const pkg = require('./package.json')

const APP_DIR = path.resolve(__dirname, 'src')
const BUILD_DIR = path.resolve(__dirname, 'public')

const config = {
  entry: path.join(APP_DIR, 'index.jsx'),

  output: {
    path: BUILD_DIR,
    filename: 'index.js'
  },

  module: {
    loaders: [
      {
        test: /\.scss$/,
        loaders: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.jsx$/,
        include: APP_DIR,
        loader: 'babel-loader'
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: pkg.description,
      author: pkg.author.name,
      template: path.join(APP_DIR, 'index.ejs'),
      inlineSource: '.(js|css)$'
      // TODO favicon: path,
    }),
    new HtmlWebpackInlineSourcePlugin()
  ]
}

module.exports = config
